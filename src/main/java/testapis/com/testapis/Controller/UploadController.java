package testapis.com.testapis.Controller;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.cloudinary.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.util.Map;
@Controller
public class UploadController {
    @PostMapping("/file/upload")
    @ResponseBody
    public ResponseEntity<String> handleFileUpload(@RequestParam(value="file",required = true) MultipartFile file) {
        try{
            Cloudinary c = new Cloudinary("cloudinary:" +
                    "//593634231832449:1LJy2z_R4JuquzHbyznsQIYKCO0@dnyoladep");
            try
            {
                File f= Files.createTempFile("temp", file.getOriginalFilename()).toFile();
                file.transferTo(f);

                Map response=c.uploader().upload(f, ObjectUtils.emptyMap());
                JSONObject json=new JSONObject(response);
                String url=json.getString("url");
                return new ResponseEntity<String>("{\"status\":\"OK\", \"url\":\""+url+"\"}", HttpStatus.OK);
            }
            catch(Exception e)
            {
                return new ResponseEntity< String >("", HttpStatus.BAD_REQUEST);
            }
        }catch(Exception Ex){
            Ex.printStackTrace();
        }

        return null;
    }
}
