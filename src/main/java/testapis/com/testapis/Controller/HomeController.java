package testapis.com.testapis.Controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import testapis.com.testapis.Database.Connections;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@Controller
public class HomeController {
    @RequestMapping(value="/",method = RequestMethod.GET)
    public String index(
    ) throws IOException, SQLException {
        return "index";
    }

    @RequestMapping(value="/{kindParam}",method = RequestMethod.POST)
    @ResponseBody
    public JsonNode test(
            @PathVariable("kindParam") String kind,
            @RequestBody String body,
            HttpServletRequest request
    ) throws IOException, SQLException {
        Connections query = new Connections();
        JsonNode resp = query.Querys(kind, body);
        System.out.println(resp.toString());
        query.con.close();

        return resp;
    }
}
