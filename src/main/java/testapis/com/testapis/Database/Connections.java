package testapis.com.testapis.Database;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Connections {
    public java.sql.Connection con ;
    private Statement stmt;
    private ResultSet rs;
    private static final Logger log = LoggerFactory.getLogger(Connections.class);

    public Connections(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        con = null;
        try {
            con = DriverManager.getConnection("jdbc:postgresql://ec2-54-227-251-233.compute-1.amazonaws.com:5432/d67c40e0arocul?autoReconnect=true&sslmode=require",
                    "ektxvivakcptka",
                    "a98f7d524bb3b2281dbdcf0e2ac7508a6880146a0b23c75f7371f47060d392a6");
            log.info("Database CONNECT");
            stmt = con.createStatement();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public JsonNode Querys(String kind, String obj) throws IOException, SQLException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode resp = null;
        String rslt = null;
        String sql = "select request('"+kind+"','"+obj+"') as response ";
        System.out.println(sql);
        try{
            rs =stmt.executeQuery(sql);
            if(rs.next()){
                rslt = rs.getString("response");
                resp = objectMapper.readTree(rslt);
            }
        }catch (SQLException sqlException){
            rslt = parseError(sqlException.getMessage());
        }finally {
            con.close();
            return resp;
        }

    }
    private String parseError(String msg){
        String value = "{\"status\" : false, \"message\" : \""+msg+"\"}";
        return value;
    }

}
